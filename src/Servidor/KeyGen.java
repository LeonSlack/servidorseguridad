/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import static org.apache.commons.codec.binary.Base64.encodeBase64;

public class KeyGen
{
	private static int DATA_SIZE = 50;
	private static String MODE = "RSA";
	private static String PROVIDER = "BC";
	
	private KeyPairGenerator kg;
	private KeyPair kp;
	private PrivateKey prk;
	private PublicKey puk;
	
	private Cipher cipher;
	
	public KeyGen()
	{
		try
		{
			kg = KeyPairGenerator.getInstance(MODE);
			kg.initialize(512);
			
			cipher = Cipher.getInstance(MODE);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public KeyGen(PublicKey puk, PrivateKey prk)
	{
		try
		{
			kg = KeyPairGenerator.getInstance(MODE, PROVIDER);
			kg.initialize(512);
			
			cipher = Cipher.getInstance(MODE, PROVIDER);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		this.puk = puk;
		this.prk = prk;
	}
	
	public byte[] encryptWithExternalPublic(byte[] s, PublicKey p)
	{
		try
		{
			cipher.init(Cipher.ENCRYPT_MODE, p);
			return cipher.doFinal(s);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public byte[] encryptWithExternalPrivate(byte[] s, PrivateKey p)
	{
		try
		{
			cipher.init(Cipher.ENCRYPT_MODE, p);
			return cipher.doFinal(s);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public byte[] decryptWithExternalPublic(byte[] s, PublicKey p)
	{
		try
		{
			cipher.init(Cipher.DECRYPT_MODE, p);
			return cipher.doFinal(s);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public byte[] decryptWithExternalPrivate(byte[] s, PrivateKey p)
	{
		try
		{
			cipher.init(Cipher.DECRYPT_MODE, p);
			return cipher.doFinal(s);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	public Cipher getCipher()
	{
		return cipher;
	}
	
	public KeyPair getKeyPair()
	{
		return this.kp;
	}
	
	public void generateKeys()
	{
		KeyPair kp = kg.generateKeyPair();
		
		this.kp = kp;
		prk = kp.getPrivate();
		puk = kp.getPublic();
	}
	
	public PrivateKey getPrivateKey()
	{
		return prk;
	}
	
	public PublicKey getPublicKey()
	{
		return puk;
	}
	
	public byte[] encryptPublic(byte[] s)
	{
		try
		{
			cipher.init(Cipher.ENCRYPT_MODE, puk);
			return cipher.doFinal(s);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public byte[] encryptPrivate(byte[] s)
	{
		try
		{
			cipher.init(Cipher.ENCRYPT_MODE, prk);
			return cipher.doFinal(s);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public byte[] decryptPublic(byte[] s)
	{
		try
		{
			cipher.init(Cipher.DECRYPT_MODE, puk);
			return cipher.doFinal(s);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public byte[] decryptPrivate(byte[] s)
	{
		try
		{
			cipher.init(Cipher.DECRYPT_MODE, prk);
			return cipher.doFinal(s);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static String byteArrayToString(byte [] buffer) 
	{
		return new String(buffer, StandardCharsets.UTF_8);
	}
	
	public static ArrayList<byte[]> largeDataToArray(byte[] largeData)
	{
		ArrayList<byte[]> data = new ArrayList<byte[]>();
		
		for (int i = 0; i < largeData.length; i += DATA_SIZE)
		{
			byte[] item = new byte[DATA_SIZE];
			
			for (int k = i; k < i+DATA_SIZE; k++)
			{
				if (k >= largeData.length) break;
				
				item[k - i] = largeData[k];
			}
			
			data.add(item);
		}
		
		return data;
	}
	
	public static byte[] arrayToLargeData(ArrayList<byte[]> arr)
	{
		byte[] data = new byte[DATA_SIZE * arr.size()];
		
		for (int i = 0; i < arr.size(); i++)
		{
			for (int k = 0; k < arr.get(i).length; k++)
			{
				if ((i * DATA_SIZE + k) < data.length)
					data[i * DATA_SIZE + k] = arr.get(i)[k];
			}
		}
		
		return data;
	}
	
	public static byte[] get32Bytes(byte[] all, int part)
	{
		byte[] res = new byte[32];
		
		for (int i = 0; i < 32; i++)
			res[i] = all[part * 32 + i];
		
		return res;
	}
	
	public static byte[] addByteArrays(byte[] first, byte[] second)
	{
		byte[] res = new byte[first.length + second.length];
		
		for (int i = 0; i < 32; i++)
		{
			if (i < first.length)
				res[i] = first[i];
			
			if (i < second.length)
				res[first.length + i] = second[i];
		}
		
		return res;
	}
	
	public static ArrayList<byte[]> doHalf(ArrayList<byte[]> arr)
	{
		ArrayList<byte[]> res = new ArrayList<byte[]>();
		
		for (int i = 0; i < arr.size(); i++)
		{
			res.add(get32Bytes(arr.get(i), 0));
			res.add(get32Bytes(arr.get(i), 1));
		}
		
		return res;
	}
	
	public static ArrayList<byte[]> undoHalf(ArrayList<byte[]> arr)
	{
		ArrayList<byte[]> res = new ArrayList<byte[]>();
		
		for (int i = 0; i < arr.size(); i+=2)
		{
			res.add(addByteArrays(arr.get(i), arr.get(i+1)));
		}
		
		return res;
	}
	
	public static ArrayList<byte[]> getDataToSend(byte[] imgBytes, PublicKey pk, KeyGen kg)
	{		
		ArrayList<byte[]> imgArray = largeDataToArray(imgBytes);
		ArrayList<byte[]> privateArray = new ArrayList<byte[]>();
		
		for (int i = 0; i < imgArray.size(); i++)
			privateArray.add( kg.encryptPrivate(imgArray.get(i)) );
		
		ArrayList<byte[]> halfArray = doHalf(privateArray);
		ArrayList<byte[]> finalArray = new ArrayList<byte[]>();
		
		for (int i = 0; i < halfArray.size(); i++)
			finalArray.add( kg.encryptWithExternalPublic(halfArray.get(i), pk) );
				
		return finalArray;
	}
	
	public static byte[] getImageBytesReceived(ArrayList<byte[]> arr, PublicKey pk, KeyGen kg)
	{
		ArrayList<byte[]> publicArray = new ArrayList<byte[]>();
		
		for (int i = 0; i < arr.size(); i++)
			publicArray.add( kg.decryptPrivate(arr.get(i)) );
		
		ArrayList<byte[]> fullArray = undoHalf(publicArray);
		ArrayList<byte[]> finalImageArray = new ArrayList<byte[]>();
		
		for (int i = 0; i < fullArray.size(); i++)
			finalImageArray.add( kg.decryptWithExternalPublic(fullArray.get(i), pk) );
		
		return arrayToLargeData(finalImageArray);
	}
	
	
	
	public static ArrayList<byte[]> getDataToSend(byte[] imgBytes, KeyGen kg)
	{		
		ArrayList<byte[]> imgArray = largeDataToArray(imgBytes);
		ArrayList<byte[]> privateArray = new ArrayList<byte[]>();
		
		for (int i = 0; i < imgArray.size(); i++)
			privateArray.add( kg.encryptPrivate(imgArray.get(i)) );
								
		return privateArray;
	}
	
	public static byte[] getImageBytesReceived1(ArrayList<byte[]> arr, PublicKey pk, KeyGen kg)
	{
		ArrayList<byte[]> publicArray = new ArrayList<byte[]>();
		
		for (int i = 0; i < arr.size(); i++)
			publicArray.add( kg.decryptWithExternalPublic(arr.get(i), pk) );
				
		
		return arrayToLargeData(publicArray);
	}
        
        //nuevo
        public String encriptarConPublica(String s)
        {
            		try
		{
			cipher.init(Cipher.ENCRYPT_MODE, puk);
                        byte[] encriptado = cipher.doFinal(s.getBytes());
			return new String(encodeBase64(encriptado));
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
        }
        
        public String encriptarConPublicaExterna(String s,PublicKey p)
        {
            		try
		{
			cipher.init(Cipher.ENCRYPT_MODE, p);
                        byte[] encriptado = cipher.doFinal(s.getBytes());
			return new String(encodeBase64(encriptado));
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
        }
        public String encriptarConPrivada(String s)
        {
            		try
		{
			cipher.init(Cipher.ENCRYPT_MODE, prk);
                        byte[] encriptado = cipher.doFinal(s.getBytes());
			return new String(encodeBase64(encriptado));
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
        }
        
        public String desencriptarConPrivada(String s)
	{
		try
		{
			
                        cipher.init(Cipher.DECRYPT_MODE, prk);
                        
                        byte[] enc =Base64.decodeBase64(s); 
                        byte[] des = cipher.doFinal(enc);

			return new String(des);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
        public String desencriptarConPublica(String s)
	{
		try
		{
			
                        cipher.init(Cipher.DECRYPT_MODE, puk);
                        
                        byte[] enc =Base64.decodeBase64(s);
                        byte[] des = cipher.doFinal(enc);

			return new String(des);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
        public String desencriptarConPublicaExterna(String s, PublicKey p)
	{
		try
		{
			
                        cipher.init(Cipher.DECRYPT_MODE, p);
                        
                        byte[] enc =Base64.decodeBase64(s);
                        byte[] des = cipher.doFinal(enc);

			return new String(des);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
        public String EncriptarKS(String texto,String secretKey) 
        {
            String encriptado = "";
            try 
            {

                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
                byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
                SecretKey key = new SecretKeySpec(keyBytes, "DESede");
                Cipher cip = Cipher.getInstance("DESede");
                cip.init(Cipher.ENCRYPT_MODE,key);
                byte[] plainTextBytes = texto.getBytes("utf-8");
                byte[] buf = cip.doFinal(plainTextBytes);
                byte[] base64Bytes = Base64.encodeBase64(buf);
                encriptado = new String(base64Bytes);

            } 
            catch (Exception ex) 
            {
                ex.printStackTrace();
            }
            return encriptado;
        }

        public String DesencriptarKS(String textoEncriptado,String secretKey)
        {
            String Desencriptado = "";
            try 
            {
                byte[] message = Base64.decodeBase64(textoEncriptado.getBytes("utf-8"));
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
                byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
                SecretKey key = new SecretKeySpec(keyBytes, "DESede");
                Cipher cip = Cipher.getInstance("DESede");
                cip.init(Cipher.DECRYPT_MODE, key);
                byte[] plainText = cip.doFinal(message);

                Desencriptado = new String(plainText, "UTF-8");

            } 
            catch (Exception ex) 
            {
                ex.printStackTrace();                
            }
            return Desencriptado;
        }
}