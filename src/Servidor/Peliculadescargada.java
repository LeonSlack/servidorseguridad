/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

/**
 *
 * @author Nupy
 */
public class Peliculadescargada {
    private String nombre;
    private int vecesDescargada;
    
    public Peliculadescargada(String nuevoNombre)
    {
            nombre = nuevoNombre; //nombre de la pelicula
            vecesDescargada=0;
    }
    
    public void Sedescargo ()
    {
        this.vecesDescargada+=1;
    }
    
    public int NumeroDeDescargas ()
    {
        return this.vecesDescargada;
    }
    
    public String getNombre()
    {
            return this.nombre;
    }
    
}
