/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.text.DecimalFormat;

/**
 *
 * @author Nupy
 */
public class escucha extends Thread {
    //private Socket clientSocket;
    private static ServerSocket serverSocket;
    private static Socket clientSocket = null;
    private List Conectados=new ArrayList();
    private List Descargadas=new ArrayList();
    private Peliculadescargada PD;
    private Peliculadescargada i;
    private ClienteConectado Cliente;
    private boolean si_esta=false;
    private int pos=-9;
    public List D= new ArrayList();
    //para datos del servidor central
    private static DataInputStream in;
    private static DataOutputStream ps;;
    private int numeroDescargas=0;
    private List<String> ips=new ArrayList();
    private static KeyGen key = new KeyGen();
    private static PrivateKey prk;
    private static PublicKey PublicaServidorCentral;
    private static String sk;
    //////
    public escucha (Socket cliente,ServerSocket serverS)
    {
        this.clientSocket = cliente;
        this.serverSocket=serverS;
    }
    
    public void run (){
        key.generateKeys();
        prk=key.getPrivateKey();
        
        while (true) {
            try {
                
                clientSocket = serverSocket.accept(); 
                
                if (clientSocket.getInetAddress().toString().contains("192.168.0.103")) //ip del servidor central
                {
                    
                    in = new DataInputStream(clientSocket.getInputStream());
                    ps = new DataOutputStream (clientSocket.getOutputStream());
                    String opcion;
                    opcion=in.readUTF();
                    switch(opcion){
                        case "peliculas":   
                            
                            String sDirectorio = "c:\\server";
                            File f = new File(sDirectorio);
                            File[] ficheros = f.listFiles();

                            for (int x=0;x<ficheros.length;x++)
                            {
                                ps.writeUTF(key.EncriptarKS(ficheros[x].getName(),sk));
                            }
                            ps.writeUTF(key.EncriptarKS("fin",sk));
                            ps.writeUTF(key.EncriptarKS(Integer.toString(NumeroDESCARGAS()),sk));
                            if (ips.isEmpty())
                            {
                                ps.writeUTF(key.EncriptarKS("vacia",sk));
                            }
                            else 
                                for (int x=0;x<ips.size();x++)
                                {
                                    ps.writeUTF(key.EncriptarKS(ips.get(x),sk));
                                }
                                ps.writeUTF(key.EncriptarKS("fin",sk));
                            ps.flush();
                            break;
                        
                        case "register":
                            //envio mi publica                                                 
                            ByteBuffer bb = ByteBuffer.allocate(4);
                            bb.putInt(key.getPublicKey().getEncoded().length);
                            ps.write(bb.array());
                            ps.write(key.getPublicKey().getEncoded());
                            ps.flush();
                            //recibo la publica del servidor
                            byte[] lenb = new byte[4];
                            in.read(lenb,0,4);
                            bb = ByteBuffer.wrap(lenb);
                            int len = bb.getInt();
                            byte[] servPubKeyBytes = new byte[len];
                            in.read(servPubKeyBytes);
                            X509EncodedKeySpec ks = new X509EncodedKeySpec(servPubKeyBytes);
                            KeyFactory kf = KeyFactory.getInstance("RSA");
                            PublicaServidorCentral = kf.generatePublic(ks);             
                            //empiezo el compendio de KS
                            sk=key.desencriptarConPrivada(in.readUTF());
                            //
                            //Empiezo a recibir el certificado
                            String fileName = in.readUTF();
                            int bytesRead;
                            OutputStream output = new FileOutputStream(fileName);
                            double size = in.readLong();
                            double i = size;
                            byte[] buffer = new byte[1024];      
                            while (size > 0 && (bytesRead = in.read(buffer, 0, (int) Math.min(buffer.length, size))) != -1) {
                                output.write(buffer, 0, bytesRead);
                                size -= bytesRead;
                            }
                            output.close();
                            //
                            
                            break;    
                    }
                    clientSocket.close();
                }
                else
                    {
                    Thread t = new Thread(new CLIENTConnectionA(clientSocket));                
                    t.start();
                    }

            } catch (Exception e) {
                System.err.println(e);
            }
            
        }
    }
    
    
    public List PELICULAS_DESCARGANDO (){
        return this.Conectados;
    } 
    
    public List PELICULAS_DESCARGADAS ()
    {
        return this.Descargadas;
    }
    
    public int NumeroDESCARGAS()
    {
        return this.numeroDescargas;
    }
    
    public class CLIENTConnectionA implements Runnable {

    private Socket clientSocket;
    private BufferedReader in = null;
    private String nombrePelicula;

    
    private CLIENTConnectionA(Socket clientSocketA) 
    {
        this.clientSocket=clientSocketA;
    }

    @Override
    public void run() {
        try {

            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String clientSelection;
            //desencriptar con mi privada
            clientSelection = in.readLine();
            sendFile(clientSelection);

        } catch (IOException ex) {
            System.err.println(ex);
            //Logger.getLogger(CLIENTConnectionA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendFile(String fileName) {
        
        try {
            
            //Manejo la conexion del cliente cuando descarga una pelicula
            ClienteConectado Cliente = new ClienteConectado (clientSocket.getInetAddress().toString(),fileName);
            Conectados.add(Cliente);
            //maneja la lectura de archivo
            File myFile = new File(fileName);
            byte[] mybytearray = new byte[(int) myFile.length()];

            FileInputStream fis = new FileInputStream(myFile);
            BufferedInputStream bis = new BufferedInputStream(fis);

            DataInputStream dis = new DataInputStream(bis);
            dis.readFully(mybytearray, 0, mybytearray.length);

            //maneja los archivos que se enviaran por el socket
            OutputStream os = clientSocket.getOutputStream();

            //envio el nombre del archivo y su tamaño al cliente
            DataOutputStream dos = new DataOutputStream(os);
            dos.writeUTF(myFile.getName());
            dos.writeLong(mybytearray.length);
            dos.write(mybytearray, 0, mybytearray.length);
            dos.flush();
            //Al llegar aqui la descarga fue exitosa y la guardo 
            //
            //Quito al cliente, ya que se termino la descarga.
            Conectados.remove(Cliente);
            
            
            this.nombrePelicula=fileName;
            if (nombrePelicula!=null)
                {
                    Iterator <Peliculadescargada> it= Descargadas.iterator();
                    while (it.hasNext()){
                        i=it.next();
                        if (i.getNombre().compareTo(nombrePelicula)==0){
                            pos=Descargadas.indexOf(i);
                            i.Sedescargo();
                            Descargadas.set(pos,i);
                            si_esta=true;
                        }
                    }
                    if (si_esta==false){
                        PD = new Peliculadescargada(nombrePelicula);
                        PD.Sedescargo();
                        Descargadas.add(PD);
                    }
                }
            
            
            numeroDescargas+=1;
            
            if (ips.contains(clientSocket.getInetAddress().toString())==false)
                ips.add(clientSocket.getInetAddress().toString());
        } 
        catch (Exception e) 
        {
            System.out.println(e);        
        }
    } 
    }
}
