/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Nupy
 */
public class Servidor {

    private static ServerSocket serverSocket;
    private static Socket clientSocket = null;
    private static Peliculadescargada P;
    private static List Descargadas=new ArrayList();
    private static List Conectados = new ArrayList();
    private static ClienteConectado C;


    public static void main(String[] args) throws IOException {
        String sDirectorio = "c:\\server";
        File f = new File(sDirectorio);
        File[] ficheros = f.listFiles();

        for (int x=0;x<ficheros.length;x++)
        {
            P=new Peliculadescargada(ficheros[x].getName());
            Descargadas.add(P);
            
        }
        
        try {
            serverSocket = new ServerSocket(6666);
            
            System.out.println("Servidor: 1 ONLINE, escuchando en el puerto 6666");
        } catch (Exception e) {
            System.err.println("Puerto Ocupado");
            System.exit(1);
        }
        escucha hiloEscuchar = new escucha (clientSocket,serverSocket);
        hiloEscuchar.start();
        String selection = "";
        InputStreamReader ipr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader (ipr);
        while (true){
            System.out.println("Introduzca un comando(man para info.)");
            selection = br.readLine();
            switch (selection){
                case "man":
                    System.out.println("Comandos disponibles:");
                    System.out.println("PELICULAS_DESCARGADAS: Muestra el numero de descargas por archivo");
                    System.out.println("PELICULAS_DESCARGANDO: Muestra las peliculas que se estan descargando al momento.");
                    break;
                case "PELICULAS_DESCARGADAS":
                    Descargadas = hiloEscuchar.PELICULAS_DESCARGADAS();
                    if (Descargadas.isEmpty()==false){
                        Iterator <Peliculadescargada> it = Descargadas.iterator();
                        while(it.hasNext()){
                            P=it.next();
                            System.out.println(P.getNombre().replace("C:\\server\\", "")+" Numero de descargas: "+P.NumeroDeDescargas());
                        }
                    }
                    else 
                        System.out.println("Todavia no se han descargado peliculas.");
                        
                    break;
                case "PELICULAS_DESCARGANDO":
                    Conectados = hiloEscuchar.PELICULAS_DESCARGANDO();
                    if (Conectados.isEmpty()==false){
                        Iterator <ClienteConectado> ite = Conectados.iterator();
                        while (ite.hasNext())
                        {
                            C=ite.next();
                            System.out.println(C.getPelicula().replace("C:\\server\\", "")+" Se esta descargando desde: "+C.getIP().replace("/",""));
                        }
                    }
                    else
                        System.out.println("Ninguna pelicula se esta descargando en este momento.");
                    break;

                    
                default:
                    System.out.println("Error de comando");
            }
        
        }

        
    }

    
}
